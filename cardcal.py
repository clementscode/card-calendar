#!/usr/bin/python
# -*- coding:utf-8 -*-
import argparse
import os
import sys
from datetime import datetime, date

script_dir = os.path.dirname(os.path.realpath(__file__))
picdir = os.path.join(script_dir, 'cards')
libdir = os.path.join(script_dir, 'lib')
if os.path.exists(libdir):
    sys.path.append(libdir)

import logging
from waveshare_epd import epd7in5b_V2
import time
from PIL import Image,ImageDraw,ImageFont
import traceback

parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", action="store_true")
parser.add_argument("-d", "--date")
parser.add_argument("-i", "--index", type=int)
parser.add_argument("-b", "--black-file")
parser.add_argument("-r", "--red-file")
parser.add_argument("-c", "--check-files", action="store_true")

args = parser.parse_args()

log_level = logging.INFO
if args.verbose:
    log_level = logging.DEBUG
logging.basicConfig(level=log_level)

file_array = [
        ('ace_of_spades.bmp', None),
        ('2_of_spades.bmp', None),
        ('3_of_spades.bmp', None),
        ('4_of_spades.bmp', None),
        ('5_of_spades.bmp', None),
        ('6_of_spades.bmp', None),
        ('7_of_spades.bmp', None),
        ('8_of_spades.bmp', None),
        ('9_of_spades.bmp', None),
        ('10_of_spades.bmp', None),
        ('jack_of_spades_black.bmp', 'jack_of_spades_red.bmp'),
        ('queen_of_spades_black.bmp', 'queen_of_spades_red.bmp'),
        ('king_of_spades_black.bmp', 'king_of_spades_red.bmp'),
        ('border.bmp', 'ace_of_hearts.bmp'),
        ('border.bmp', '2_of_hearts.bmp'),
        ('border.bmp', '3_of_hearts.bmp'),
        ('border.bmp', '4_of_hearts.bmp'),
        ('border.bmp', '5_of_hearts.bmp'),
        ('border.bmp', '6_of_hearts.bmp'),
        ('border.bmp', '7_of_hearts.bmp'),
        ('border.bmp', '8_of_hearts.bmp'),
        ('border.bmp', '9_of_hearts.bmp'),
        ('border.bmp', '10_of_hearts.bmp'),
        ('jack_of_hearts_black.bmp', 'jack_of_hearts_red.bmp'),
        ('queen_of_hearts_black.bmp', 'queen_of_hearts_red.bmp'),
        ('king_of_hearts_black.bmp', 'king_of_hearts_red.bmp'),
        ('border.bmp', 'ace_of_diamonds.bmp'),
        ('border.bmp', '2_of_diamonds.bmp'),
        ('border.bmp', '3_of_diamonds.bmp'),
        ('border.bmp', '4_of_diamonds.bmp'),
        ('border.bmp', '5_of_diamonds.bmp'),
        ('border.bmp', '6_of_diamonds.bmp'),
        ('border.bmp', '7_of_diamonds.bmp'),
        ('border.bmp', '8_of_diamonds.bmp'),
        ('border.bmp', '9_of_diamonds.bmp'),
        ('border.bmp', '10_of_diamonds.bmp'),
        ('jack_of_diamonds_black.bmp', 'jack_of_diamonds_red.bmp'),
        ('queen_of_diamonds_black.bmp', 'queen_of_diamonds_red.bmp'),
        ('king_of_diamonds_black.bmp', 'king_of_diamonds_red.bmp'),
        ('ace_of_clubs.bmp', None),
        ('2_of_clubs.bmp', None),
        ('3_of_clubs.bmp', None),
        ('4_of_clubs.bmp', None),
        ('5_of_clubs.bmp', None),
        ('6_of_clubs.bmp', None),
        ('7_of_clubs.bmp', None),
        ('8_of_clubs.bmp', None),
        ('9_of_clubs.bmp', None),
        ('10_of_clubs.bmp', None),
        ('jack_of_clubs_black.bmp', 'jack_of_clubs_red.bmp'),
        ('queen_of_clubs_black.bmp', 'queen_of_clubs_red.bmp'),
        ('king_of_clubs_black.bmp', 'king_of_clubs_red.bmp'),
        ('batman_joker_black.bmp', 'batman_joker_red.bmp')
]

if args.check_files:
    for files in file_array:
        black_file = os.path.join(picdir, files[0])
        if not os.path.exists(black_file):
            logging.error(black_file + " does not exist!")
            sys.exit(1)
        if files[1]:
            red_file = os.path.join(picdir, files[1])
            if not os.path.exists(red_file):
                logging.error(files[1] + " does not exist!")
                sys.exit(1)

if args.index:
    index = args.index
else:
    if args.date:
        day = datetime.fromisoformat(args.date)
    else:
        day = date.today()
    index = day.isocalendar().week - 1

if index > len(file_array):
    logging.error("invalid index: " + str(index))
    sys.exit(1)

logging.debug("index: " + str(index))

files = file_array[index]
if args.black_file:
    black_file = args.black_file
else:
    black_file = os.path.join(picdir, files[0])
logging.debug("black file: " + black_file)
if not os.path.exists(black_file):
    logging.error("black file does not exist at " + black_file)

red_file = None
if args.red_file:
    red_file = args.red_file
elif files[1]:
    red_file = os.path.join(picdir, files[1])

if red_file:
    if not os.path.exists(red_file):
        logging.error("red file does not exist at " + red_file)
    else:
        logging.debug("red file: " + red_file)
else: 
    logging.debug("no red file")

try:
    epd = epd7in5b_V2.EPD()
    logging.debug("init")
    epd.init()

    logging.debug("read files")
    Himage = Image.open(black_file)
    if red_file:
        Himage_Other = Image.open(red_file)
    else:
        Himage_Other = Image.new('1', (epd.height, epd.width), 255)
    logging.debug("display files")
    epd.display(epd.getbuffer(Himage),epd.getbuffer(Himage_Other))

    logging.debug("Goto Sleep...")
    epd.sleep()
    
except IOError as e:
    logging.error(e)
    
except KeyboardInterrupt:    
    logging.info("ctrl + c:")
    epd7in5b_V2.epdconfig.module_exit()
    exit()
