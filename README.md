# Card Calendar

Displays current week of the year as a playing card on an e-ink display.

Documentation for display at https://www.waveshare.com/wiki/7.5inch_e-Paper_HAT_(B)_Manual#Working_With_Raspberry_Pi

The card images are separated out into separate bitmaps for the black and red pixels.
